# iOS platform
ionic cordova platform rm ios
ionic cordova platform add ios@latest
ionic cordova resources ios
ionic cordova resources ios -i -f
ionic cordova build ios --prod --aot --minifyjs --minifycss --optimizejs
