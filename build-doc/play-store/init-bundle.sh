# Inside android
cd thatbookingapp/platforms/android

# Make dir of debug
mkdir app/src/debug

# Make dir of debug
mkdir app/src/release

# Copy google-services.json file
yes | cp -rf ../../../resources/google-services.json app/src/debug
yes | cp -rf ../../../resources/google-services.json app/src/release

# Copy google-services.json file
yes | cp -rf ../../../resources/release-key.keystore .
yes | cp -rf ../../../resources/release-key.keystore app

# Create gradle.properties
touch gradle.properties
echo "org.gradle.daemon=true" >> gradle.properties
echo "org.gradle.jvmargs=-Xmx2048m" >> gradle.properties
echo "android.useDeprecatedNdk=true" >> gradle.properties
echo "cdvVersionCode=0" >> gradle.properties
echo "cdvMinSdkVersion=19" >> gradle.properties
echo "cdvTargetSdkVersion=28" >> gradle.properties
echo "MYAPP_UPLOAD_STORE_FILE=release-key.keystore" >> gradle.properties
echo "MYAPP_UPLOAD_KEY_ALIAS=booking" >> gradle.properties
echo "MYAPP_UPLOAD_STORE_PASSWORD=weppsol" >> gradle.properties
echo "MYAPP_UPLOAD_KEY_PASSWORD=weppsol" >> gradle.properties


# Add signingConfigs object inside android
count=1
lineNumber=`awk '/android {/{ print NR; }' app/build.gradle`
addLine=`expr $lineNumber + $count`
ex -s -c  $addLine"i|
    signingConfigs {
        release {
            if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
                storeFile file(MYAPP_UPLOAD_STORE_FILE)
                storePassword MYAPP_UPLOAD_STORE_PASSWORD
                keyAlias MYAPP_UPLOAD_KEY_ALIAS
                keyPassword MYAPP_UPLOAD_KEY_PASSWORD
            }
        }
    }" -c x app/build.gradle
######################## End of signingConfigs ########################


# thatbookingapp/platforms/android/app/build.gradle
# thatbookingapp/platforms/android/gradle.properties
# thatbookingapp/platforms/android/app/build/outputs/bundle/debug/app.aab
