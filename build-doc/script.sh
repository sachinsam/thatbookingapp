# Git clone repo
# git clone https://gitlab.com/paragshelke30_weppsol/bookingmobileapp.git
git clone https://gitlab.com/wep_sachin/thatbookingapp.git

# Inside the git repo
cd thatbookingapp/

# Git pull
# git pull origin hotfix_tp467

#Current Path
pwd

# Generate key
# keytool -genkey -v -keystore ./build-doc/play-store/keystore/release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias booking
# Enter key password for <booking> : weppsol

# Run before build script
sh ../build.sh

# Update spalsh screen and icon
cp -f ../resources/splash.png resources/splash.png
cp -f ../resources/icon.png resources/icon.png

# Update FCM configuration
cp -f ../resources/google-services.json google-services.json

# Update background image
cp -f ../resources/yoga.jpg src/assets/imgs/yoga.jpg
cp -f ../resources/yoga1.jpg src/assets/imgs/yoga1.jpg

cp -f ../resources/yoga.jpg src/assets/imgs/HeatherWax_Splash_1.jpg

# Run android script
sh build-doc/play-store/android.sh
