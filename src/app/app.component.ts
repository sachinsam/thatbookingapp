import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CommonProvider } from './services/common/common';
import { AppSettings } from './services/app-settings';
import { Router } from '@angular/router';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import * as _ from 'lodash';

import { Location } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  label: any;
  wifiSvgIcon: any;
  errorFlag: boolean = false;
  rootPage: any;
  rootPageParams: any;

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  public lastBack ;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public commonprovider: CommonProvider,
    public appSettings: AppSettings,
    private appVersion: AppVersion,
    public fcm: FCM,
    public router:Router,public alert:AlertController,
    public location:Location
    
  ) 
  {
    this.label = this.commonprovider.getLabels().app;
    this.wifiSvgIcon = this.commonprovider.getSvgIcos().wifi;

    
    this.initializeApp();
    /*this.platform.ready().then(() => {
      console.log("testing");
      //this.initializeCustomApp();
      //https://forum.ionicframework.com/t/how-to-conditionally-set-root-page-in-ionic-4/138592/7
      
      this.sleep(5000); 
      
       this.commonprovider.setisiOSplt(false);

      if (platform.is('ios')) {
        statusBar.overlaysWebView(false);
        this.commonprovider.setisiOSplt(true);
      }

      let primaryColor = this.commonprovider.getColorCode('--primary-color');
      statusBar.backgroundColorByHexString(primaryColor);
      //splashScreen.hide();

      this.initializeFCM();

      this.getConfig();
    });
      */
  }

  initializeCustomApp()
  {
    //this.splashScreen.hide();

    this.commonprovider.getUserData().then(userData => {
      if (userData) {
        this.commonprovider.setUserData(userData);
        //this.navCtrl.setRoot(DashboardPage, { userdata: userData });
        this.router.navigateByUrl('/');
      } else {
        //this.rootPage = LoginPage;
        this.router.navigate(["login"]);
        this.commonprovider.hideLoader();
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //this.statusBar.styleDefault();
      //call config here

      this.commonprovider.setisiOSplt(false);

      if (this.platform.is('ios')) {
        this.statusBar.overlaysWebView(false);
        this.commonprovider.setisiOSplt(true);
      }

      let primaryColor = this.commonprovider.getColorCode('--ion-color-primary');
      primaryColor = primaryColor.trim().replace("#","")
      console.log("color is "+primaryColor);
      this.statusBar.backgroundColorByHexString("#"+primaryColor);
      //splashScreen.hide();


      this.subscribeToBackButton();

       this.initializeFCM();
 
       //this.getConfig(); 

     // this.splashScreen.hide();//hide 
    });


  }

  initializeFCM()
  {
    // FCM code
      if (this.platform.is('ios') || this.platform.is('android')) {
        this.fcm.subscribeToTopic('/topics/all');
        this.fcm.getToken().then(token => {
          this.commonprovider.setFCMToken(token);
          console.log("Device token from fcm is ", token)
        });
      }

      this.fcm.onNotification().subscribe(data => {
       console.log("data in foreground ", data);
       if (this.platform.is('ios')) {
         if (data.wasTapped) {
           this.commonprovider.showToast(data.aps.alert.body, 5000, 'top');
         }else{
           this.commonprovider.showToast(data.aps.alert.body, 5000, 'top');
         }
       }else{
         if (data.wasTapped) {
           this.commonprovider.showToast(data.body, 5000, 'top');
         }else{
           this.commonprovider.showToast(data.body, 5000, 'top');
         }
       }
     });

  }

  getConfig() {
    let packageName = 'com.limedrive.tp408';

    this.commonprovider.showLoader('Initializing App ...');

    this.appVersion.getPackageName()
      .then((resPackageName) => {
        packageName = resPackageName;

        // Set in build.sh shell script file for dynamic URL
        // this.appSettings.set_API_ENDPOINT('https://' + packageName + '/api');

        let devicePlatform = this.platform.is('ios') ? 'ios' : 'android';
        this.commonprovider.setPackageName(packageName, devicePlatform);
        this.callGetConfig(packageName);
      })
      .catch((err) => {
        console.log("Error:", err);
        this.commonprovider.showToast('Error !');
        this.callGetConfig(packageName); // For web
		
      });
  }

  callGetConfig(packageName: string) 
  {
    let URL = this.appSettings.getAppConfig + '?pkgname=' + packageName;

    this.commonprovider
      .get(URL)
      .subscribe(
        (response: any) => {
          this.errorFlag = false; 
          if (!response.err_code && response.data && response.data.config) {
            this.commonprovider.setConfig(response.data.config);
            this.setConfigSync(response.data.config);
            //this.initializeApp();
            //this.initializeCustomApp(); //since we handle the routing now
            this.commonprovider.hideLoader();
            //hide splash here :) 
            this.splashScreen.hide();

          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(response.err_msg || 'Error in config', 5000);
          }
        },
        err => {
          this.errorFlag = true;
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );
  }

  setConfigSync(config) {
    let label = this.commonprovider.getLabels();
    this.commonprovider.setLabels(_.merge(label, config.label));
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

  subscribeToBackButton()
  {
    this.platform.backButton.subscribeWithPriority(1,async()=>{
  
      if(this.router.url === "/dashboard" || this.router.url === "/pages/dashboard" || this.router.url === "/login")
      {
        
        if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
          // this.platform.exitApp(); // Exit from app
          //navigator['app'].exitApp(); // work in ionic 4
          this.presentAlert("Exit", "Do you want to exit the app?");

        } 
        //else {
         // this.commonprovider.showAlert("", "Press back again to exit App");
          
          this.lastTimeBackPress = new Date().getTime();
          //event.stopPropagation();
          //event.preventDefault();
  
        //}
        
      
      }
      else
        {
          //check check if its a page or popup and then decide 
          this.location.back(); ///for page
        }

    });
    

   this.platform.backButton.subscribe(async ()=>{
    if(this.router.url=="login"){
      navigator['app'].exitApp();
    }
    if (Date.now() - this.lastBack < 500) {
      this.presentAlert("Exit", "Do you want to exit the app?");
     // navigator['app'].exitApp();
    }
    this.lastBack = Date.now();
  });

  }

  onYesHandler(){
    navigator['app'].exitApp();  
  }
  onNoHandler(){
    this.alert.dismiss();
  }
  async presentAlert(type,message) {
    const alert = await this.alert.create({
      header: type,
      message: message,
      buttons: [
        {
          text:'Yes',
          handler:this.onYesHandler
        },
        {
          text:'No',
          handler:async()=>{
            this.alert.dismiss();
          }
        }
      ],
      animated:true
    });

     await alert.present();
  }
 
}
