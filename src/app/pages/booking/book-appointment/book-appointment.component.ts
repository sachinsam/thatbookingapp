import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { InAppBrowserOptions,InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AlertController, ModalController, Events } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { DateComponent } from '../../date/date.component';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ServiceComponent } from '../../service/service.component';
import { TherapistComponent } from '../../therapist/therapist.component';
import { TimeslotsComponent } from '../../timeslots/timeslots.component';
import { CalendarComponent } from '../../date/calendar/calendar.component';

@Component({
  selector: 'app-book-appointment',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.scss'],
})
export class BookAppointmentComponent implements OnInit {
  //@ViewChild(DateComponent) dateModel: DateComponent;

  services: any[];
  therapists: any[];
  countries: any[];
  userData: any;
  minDate: any;
  appointmentDate: any;
  errMsg: boolean = false;
  autoFill: boolean = true;
  selectedTherapist: any = {
    id: ''
  };

  selectedService: any = {
    service: '',
    id: ''
  };

  selectedCountry: any = {
    name: ''
  };

  calendarConfig: any = {
    type: 'list',
    title: 'Select Date',
    icon: 'md-calendar',
    pickMode: 'single',
    closeLabel: 'Cancel',
    doneLabel: 'Select',
    disableWeeks: [],
    serviceData:[],
    therapistData:[],
  };

  bookingForm = this.fb.group({
    serviceName: new FormControl('', Validators.required),
    sid: new FormControl('', Validators.required),
    did: new FormControl('', Validators.required),
    booking_date: ['', Validators.required],
    booking_start_time: new FormControl('', Validators.required),
    booking_end_time: new FormControl('', Validators.required),
    booking_total: new FormControl('0.00', Validators.required),
    booking_amount: new FormControl('0.00', Validators.required),
    booking_notw: [''],
    cnaame: ['', Validators.required],
    cemail: ['', Validators.required],
    cphone_code: ['91', Validators.required], // Hard
    cphone_no: ['', Validators.required],
    caddress: ['', Validators.required],
    ccity: ['', Validators.required],
    czip: ['', Validators.required],
  });

  label: any;
  config: any;

  options: InAppBrowserOptions = {
    location: 'no',
    clearcache: 'yes',
    zoom: 'no',
    hardwareback: 'yes',
    toolbar: 'yes',
    toolbarcolor: '#488aff',
    hidenavigationbuttons: 'yes',
    hidespinner: 'no',
    toolbarposition: 'top',
    allowInlineMediaPlayback: 'no',
    closebuttoncaption: '< Back',
    closebuttoncolor: '#FFFFFF'
  };


  public passedObj:any;

  constructor(    private fb: FormBuilder,
    public alerCtrl: AlertController,
    public modal: ModalController,
    public datepipe: DatePipe,
    public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public events: Events,
    public iab: InAppBrowser,
    public route:ActivatedRoute,
      private router: Router
) 
{ 

  this.route.queryParams.subscribe(params => {
    if (this.router.getCurrentNavigation().extras.state) {
      this.passedObj = this.router.getCurrentNavigation().extras.state.userdata;
    }
  });

  this.therapists = [];
    this.options.toolbarcolor = this.commonprovider.getColorCode('--primary-color');
    this.options.closebuttoncolor = this.commonprovider.getColorCode('--nbg1-color');
}

  ngOnInit() 
  {
    this.dataInit();
    this.eventSubscribe();

    this.label = this.commonprovider.getLabels().booking;
    this.config = this.commonprovider.getConfig();
    this.calendarConfig.title = this.label.calendarDate;
    this.calendarConfig.closeLabel = this.label.closeLabel;
    this.calendarConfig.doneLabel = this.label.doneLabel;
  }

  ngOnDestroy(): void {
    this.events.unsubscribe('date:selected');
  }

  dataInit() 
  {
    this.userData = this.commonprovider.getUserDataGlobal();
    
 //   let serviceFlow = this.navParams.get('serviceFlow');

    let serviceFlow = this.passedObj.serviceFlow;

    if (!serviceFlow) 
    {
      let data = this.passedObj.data ;
      this.setServiceData(data.selectedService);
      this.setTherapistData(data.selectedTherapist);
      //this.dateModel.openCalendar(this.calendarConfig);//by prashant

    } else {
      this.showServices();
    }

    this.countries = [];
    this.minDate = new Date();
    this.appointmentDate = new Date();
    let dte = this.datepipe.transform(this.appointmentDate, 'yyyy-MM-dd');
    this.bookingForm.controls['booking_date'].setValue(dte);
    this.setPatientDetails();
  }

  eventSubscribe() {
    this.events.subscribe('date:selected', (dateFilter) => {
      if (dateFilter) {
        this.setDate(dateFilter.selectedDate);
        this.setTime(dateFilter.selectedTime) ;
      }
    });
  }

  setPatientDetails() {
    let user_info = this.userData.user_info;
    this.bookingForm.controls['cnaame'].setValue(this.userData.name);
    this.bookingForm.controls['cemail'].setValue(user_info.order_email);
    this.bookingForm.controls['cphone_no'].setValue(user_info.order_phone);
    this.bookingForm.controls['caddress'].setValue(user_info.order_address);
    this.bookingForm.controls['ccity'].setValue(user_info.order_city);
    this.bookingForm.controls['czip'].setValue(user_info.order_zip);
  }

  ionViewDidLoad() { }

  onSubmit() {
    let formObj = this.bookingForm.value;
    formObj.uid = this.commonprovider.getUserLoginId();
    delete formObj.serviceName;

    /*
        if (!this.bookingForm.controls['serviceName'].value) {
          this.commonprovider.showToast(this.label.msgServiceName);
          return false;
        }
        if (!this.bookingForm.controls['booking_date'].value) {
          this.commonprovider.showToast(this.label.msgBookingDate);
          return false;
        }
        if (!this.bookingForm.controls['booking_start_time'].value) {
          this.commonprovider.showToast(this.label.msgSelectTime);
          return false;
        }
        if (!this.bookingForm.controls['booking_total'].value) {
          this.commonprovider.showToast(this.label.msgBookingTotal);
          return false;
        }
        if (!this.bookingForm.controls['cnaame'].value) {
          this.commonprovider.showToast(this.label.msgCname);
          return false;
        }
        if (!this.bookingForm.controls['cemail'].value) {
          this.commonprovider.showToast(this.label.msgCemail);
          return false;
        }
        if (!this.bookingForm.controls['cphone_no'].value) {
          this.commonprovider.showToast(this.label.msgCphoneNo);
          return false;
        }
        if (!this.bookingForm.controls['caddress'].value) {
          this.commonprovider.showToast(this.label.msgCaddress);
          return false;
        }

        if (!this.bookingForm.controls['ccity'].value) {
          this.commonprovider.showToast(this.label.msgCcity);
          return false;
        }
        if (!this.bookingForm.controls['czip'].value) {
          this.commonprovider.showToast(this.label.msgCzip);
          return false;
        }
     */

    this.commonprovider.showLoader(this.appsettings.bookLoader);
    this.commonprovider.post(this.appsettings.postBooking, formObj).subscribe(
      (response: any) => {
        this.commonprovider.hideLoader();
        if (!response.err_code) {
          if (!response.data.success) {
            this.commonprovider.showToast(response.data.err_msg, 5000);
          } else {
            // this.commonprovider.showToast(this.label.msgSuccess, 5000);
            this.openInAppBrowser(response);
          }
        } else {
          this.commonprovider.showToast(response.err_msg, 5000);
          this.commonprovider.hideLoader();
        }
      },
      err => {
        this.commonprovider.hideLoader();
        this.commonprovider.showToast(err.message || this.label.msgError);
      }
    );
  }

  openInAppBrowser(response) {
    let paymentStatus: any;

    const browser =
      this.iab
        .create(
          response.data.payment_url,
          "_blank",
          this.options // no spaces allowed in options!
        );

    browser.on('loadstop').subscribe((e) => {
      paymentStatus = this.getQueryString('app_call', e.url);
      if (paymentStatus != null) {
        setTimeout(() => {
          browser.close();
        }, 2000)
      }
    });

    browser.on('exit').subscribe((e) => {
      this.paymentDone(paymentStatus);
    });
  }

  getQueryString(field: any, url: any) {
    let href = url;
    let reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    let string = reg.exec(href);
    return string ? string[1] : null;
  };

  paymentDone(status: any) {
    if (status != null) {
      status ? this.commonprovider.showToast(this.label.msgPaymentSuccess) : this.commonprovider.showToast(this.label.msgPaymentError);
      this.showPaymentStatus(status);
    } else {
      this.commonprovider.showToast(this.label.msgPaymentError);
    }
  }

  showPaymentStatus(status) {
    let formObj = this.bookingForm.value;
    let obj = {
      status: status,
      data: {
        appointment: formObj.cnaame,
        service_name: formObj.serviceName,
        booking_date: formObj.booking_date,
        time: formObj.booking_start_time + ' - ' + formObj.booking_end_time
      }
    };

    if (status) {
      obj.data['fees'] = this.config.currency_symbol + formObj.booking_amount;
      obj.data['note'] = formObj.booking_notw;
      obj.data['email'] = formObj.cemail;
    }

    //this.navCtrl.push(SuccessPage, { 'paymentResponse': obj });

    let navigationExtras: NavigationExtras = {
      state: {
        paymentResponse: obj
      }
    };

    this.router.navigate(["pages",'success'],navigationExtras);


  }

  async showServices(ev?: any) {
    let servId: any;
    this.selectedService.id
      ? (servId = this.selectedService.id)
      : (servId = null);


    const srvcModal = await this.modal.create({
      component:ServiceComponent,
      componentProps:{'id':servId}
    });

    srvcModal.onDidDismiss().then((data) => {
      
      if (data && data.data != undefined) {
        this.setServiceData(data.data);
        this.showTherapist();
      }

    });
 
    return await srvcModal.present();


  }

  async showTherapist(ev?: any) {
    let obj: any = {
      therapistId: this.selectedTherapist.id,
      selectedService: this.selectedService
    };


    const therapistModal = await this.modal.create({
      component:TherapistComponent,
      componentProps:{data: obj}
    });

    await therapistModal.present();

    therapistModal.onDidDismiss().then((data) => {
      
      if (data && data.data != undefined) {
        this.setTherapistData(data.data);
        //this.dateModel.openCalendar(this.calendarConfig);//@prashant
      }

    });


  }

  setTherapistData(data: any) {
    this.selectedTherapist = data;
    this.calendarConfig.disableWeeks = data.weekDays;
    this.bookingForm.controls['did'].setValue(data.id);
    
    //@veramatic
    this.calendarConfig.therapistData = data;

  }

  setDate(dte: any) {
    this.appointmentDate = new Date(dte);
    let dt = this.datepipe.transform(this.appointmentDate, 'yyyy-MM-dd');
    this.bookingForm.controls['booking_date'].setValue(dt);
    
    /**
     * @veramatic : commented by veramatic as the beheviour has chane to show calendar and timeslot in one modal itself
     * set time here
     * this.showTimeSlots();
     */
    
  }

  setTime(bookTime)
  {
    if (bookTime) 
        {
          let endTime = this.selectedService.service_length; // Need to remove 00:
          this.bookingForm.controls['booking_start_time'].setValue(bookTime);
          this.bookingForm.controls['booking_end_time'].setValue(this.addTimes(bookTime, endTime));
        }
  }

  setServiceData(data: any) {
    this.selectedService = data;
    this.bookingForm.controls['serviceName'].setValue(data.service_name);
    this.bookingForm.controls['sid'].setValue(data.id);
    this.bookingForm.controls['booking_total'].setValue(data.service_price);
    this.bookingForm.controls['booking_amount'].setValue(data.service_price);

    //@veramatic
    this.calendarConfig.serviceData = data;

  }

  showProfile(event: any, therapistObj: any) {
    event.stopPropagation();

    //this.navCtrl.push(ProfilePage, { therapistProfile: therapistObj });

    let navigationExtras: NavigationExtras = {
      state: {
        therapistProfile: therapistObj
      }
    };

    this.router.navigate(["pages",'profile'],navigationExtras);


  }

  async showTimeSlots(evnt) {
    if (this.selectedTherapist.id !== '') 
    {

      const options: any = {
        autoDone: true,
        pickMode: this.config.pickMode,
        title: this.config.title,
        closeLabel: this.config.closeLabel,
        doneLabel: this.config.doneLabel,
        defaultScrollTo: new Date(),
        //defaultDateRange: this.dateRange,
        //disableWeeks: this.config.disableWeeks
      //     daysConfig: _daysConfig
        serviceData:this.calendarConfig.serviceData,
        therapistData:this.calendarConfig.therapistData,
        booking_date: this.bookingForm.value.booking_date,
        bookTime: this.bookingForm.value.booking_start_time,
            
      };

      //@veramatic : To keep call same as the one from date . 
      const timeslotModal = await this.modal.create({
        //component:TimeslotsComponent,
        component:CalendarComponent,
        componentProps:{options}
      });

      /*const timeslotModal = await this.modal.create({
        //component:TimeslotsComponent,
        component:CalendarComponent,
        componentProps:{
          booking_date: this.bookingForm.value.booking_date,
          serviceId: this.selectedService.id,
          bookTime: this.bookingForm.value.booking_start_time,
          therapistId: this.selectedTherapist.id
        }
      });*/
  
      await timeslotModal.present();

      timeslotModal.onDidDismiss().then((bookTime) => {
      
        if (bookTime.data != undefined) 
        {
          let endTime = this.selectedService.service_length; // Need to remove 00:
          this.bookingForm.controls['booking_start_time'].setValue(bookTime.data.selectedTime);
          this.bookingForm.controls['booking_end_time'].setValue(this.addTimes(bookTime.data.selectedTime, endTime));
        }

      });



    } else {
      this.commonprovider.showToast(this.label.msgTherapist);
      return 0;
    }
  }

  // Convert a time in hh:mm format to minutes
  timeToMins(time) {
    var b = time.split(':');
    return b[0] * 60 + +b[1];
  }

  // Convert minutes to a time in format hh:mm
  // Returned value is in range 00  to 24 hrs
  timeFromMins(mins) {
    function z(n) { return (n < 10 ? '0' : '') + n; }
    var h = (mins / 60 | 0) % 24;
    var m = mins % 60;
    return z(h) + ':' + z(m);
  }

  // Add two times in hh:mm format
  addTimes(t0, t1) {
    return this.timeFromMins(this.timeToMins(t0) + this.timeToMins(t1));
  }

  showWarning() {
    this.commonprovider.showToast(this.label.msgService);
  }

  changeAutoFill() {
    this.autoFill = !this.autoFill;
    this.autoFill ? this.setPatientDetails() : this.resetPatientDetails();
  }

  resetPatientDetails() {
    this.bookingForm.controls['cnaame'].reset();
    this.bookingForm.controls['cemail'].reset();
    this.bookingForm.controls['cphone_no'].reset();
    this.bookingForm.controls['caddress'].reset();
    this.bookingForm.controls['ccity'].reset();
    this.bookingForm.controls['czip'].reset();
  }

}
