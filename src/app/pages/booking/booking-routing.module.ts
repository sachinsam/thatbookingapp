import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookingPage } from './booking.page';
import { AppointmentComponent } from './appointment/appointment.component';
import { UserbookingComponent } from './userbooking/userbooking.component';
import { BookingdataResolverService } from 'src/app/services/bookingdata-resolver.service';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';

const routes: Routes = [
  {
    path: '',
    component: BookingPage,
    resolve:{special:BookingdataResolverService},
    children:[
      //{ path:'', redirectTo:'appointment', pathMatch:'full'},
      { path : 'appointment' , component:AppointmentComponent},
      { path : 'userbooking',component:UserbookingComponent},
      //{ path : 'bookAppointment',component:BookAppointmentComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookingPageRoutingModule {}
