import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookingPageRoutingModule } from './booking-routing.module';

import { BookingPage } from './booking.page';
import { AppointmentComponent } from './appointment/appointment.component';
import { UserbookingComponent } from './userbooking/userbooking.component';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { BookingWizardComponent } from './booking-wizard/booking-wizard.component';
import {MatStepperModule} from '@angular/material/stepper';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookingPageRoutingModule,
    SuperTabsModule,
    MatStepperModule
  ],
  declarations: [BookingPage,UserbookingComponent,AppointmentComponent]
})
export class BookingPageModule {}
