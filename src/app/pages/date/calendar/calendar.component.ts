import { Component, OnInit, Input } from '@angular/core';
import { AppSettings } from 'src/app/services/app-settings';
import { CommonProvider } from 'src/app/services/common/common';
import { NavParams, ModalController } from '@ionic/angular';
import { BookimgWizardDataService } from 'src/app/services/bookimg-wizard-data.service';
import { CalendarModalOptions, DayConfig, CalendarComponentOptions } from 'ion2-calendar';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})

export class CalendarComponent implements OnInit {
  date: string;
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'

  calendarCm:any;
  calOptions: CalendarComponentOptions  = {
    /*autoDone: true,
    pickMode: this.config.pickMode,
    title: this.config.title,
    closeLabel: this.config.closeLabel,
    doneLabel: this.config.doneLabel,
    from: startDate,
    to: endDate,
    defaultScrollTo: new Date(),
    //defaultDateRange: this.dateRange,
    //disableWeeks: this.config.disableWeeks
    //     daysConfig: _daysConfig
  */
  disableWeeks:[0,5]

  };

  //
  config: any;
  booking_date: any;
  serviceId: any;
  therapistId: any;
  available_timeslot: any = [];
  unavailable_timeslot: any = [];
  selectedTimeslot: any;
  label: any;
  timeNotFound: boolean = false;

  timeSlots:any=[];


  serviceData:any;
  therapistData:any;
  
  //wizard related variable
  @Input() frmWizard:boolean = false ;

  @Input() options  ;

  constructor(
      public appsettings: AppSettings,
      public commonprovider: CommonProvider,
      //public params: NavParams,
      public view:ModalController,
      public bookingwzrd:BookimgWizardDataService
      ) 
      { 
        this.label = this.commonprovider.getLabels().serviceTimeslots;
      }

  ngOnInit() 
  {
    //this.label = this.commonprovider.getLabels().serviceTimeslots;

    if(this.frmWizard)
    {
      this.bookingwzrd.therapistData.subscribe((obj)=>{
        this.getTimeByTherapistService(undefined,obj.selectedTherapist) ;
      });

      this.bookingwzrd.serviceData.subscribe((obj)=>{
        if(obj.data != undefined)
        {
          this.getTimeByTherapistService(obj.data.selectedService,undefined) ;
        }
        else
          this.getTimeByTherapistService(obj.selectedService,undefined) ;
      });
    }

    if(this.options != undefined)
    {
        //this.serviceData = this.params.get("options").serviceData;
      this.serviceData = this.options.serviceData;
      this.serviceId = this.serviceData.id ;
      
      //this.therapistData = this.params.get("options").therapistData;
      this.therapistData = this.options.therapistData;
      this.therapistId = this.therapistData.id ;

      this.dataInit();
    }
  
    
  }

  onChange($event) 
  {
    //console.log($event);
    //console.log("Day is "+$event._d.getDate()+" and month is "+($event._d.getMonth()+1)+" and year is "+$event._d.getFullYear());
    this.booking_date = $event._d.getFullYear()+"-"+("0"+($event._d.getMonth()+1)).slice(-2)+"-"+("0" + $event._d.getDate()).slice(-2) ;

    this.getTimeSlots();
  }

  onMnthChange($event) 
  {
    //console.log($event);
    //console.log("Day is "+$event._d.getDate()+" and month is "+($event._d.getMonth()+1)+" and year is "+$event._d.getFullYear());
    //this.booking_date = $event._d.getFullYear()+"-"+("0"+($event._d.getMonth()+1)).slice(-2)+"-"+("0" + $event._d.getDate()).slice(-2) ;

    let selectedMonth = ("0"+($event.newMonth.dateObj.getMonth()+1)).slice(-2) ;
    let year = $event.newMonth.dateObj.getFullYear() ;
    //console.log("month="+selectedMonth);
    this.getCalendarMeta(selectedMonth,year);
  }
  
  dataInit() 
  {
    this.config = this.commonprovider.getConfig();
    //this.booking_date = this.params.get('options').booking_date;
    this.booking_date = this.options.booking_date;

    //this.serviceId = this.navParams.get('serviceId');
    //this.selectedTimeslot = this.params.get('options').bookTime;
    this.selectedTimeslot = this.options.bookTime;
    //this.therapistId = this.navParams.get('therapistId');

    if(this.booking_date != undefined)
    {
      this.date = this.booking_date;
      this.getTimeSlots();
    
    }
  }

  getTimeSlots() 
  {
    let dt = this.booking_date.split('-');
    this.commonprovider.showLoader(this.label.getTimemsg);
    this.commonprovider
      .get(
        this.appsettings.getTherapistsList
        + '?uid=' + this.config.therapist
        + '&sid=' + this.serviceId
        + '&year=' + dt[0]
        + '&month=' + dt[1]
        + '&day=' + dt[2]
        + '&eid=' + this.therapistId
      )
      .subscribe(
        (resp: any) => {
          if (!resp.err_code && resp.data) {
            let employee = resp.data.employees[0]; // Hard code
            if(employee)
            {
              this.timeSlots = employee.timeslots;
              //this.available_timeslot = employee.timeslots[0].available_timeslot;
              //this.unavailable_timeslot = employee.timeslots[0].unavailable_timeslot;
            }else{
              this.timeNotFound = true;
            }
            this.commonprovider.hideLoader();
          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(resp.err_msg);
          }
        },
        error => {
          this.commonprovider.showToast(error.message);
          this.commonprovider.hideLoader();
        }
      );
  }

  selectTime(index: any, data,selectedDate) {
    this.selectedTimeslot = data;
    this.booking_date = selectedDate;
    this.setTime(selectedDate);
  }

  setTime(selectedDate) 
  {
    if(!this.frmWizard)
    {
      this.view.dismiss({"selectedDate":selectedDate,"selectedTime":this.selectedTimeslot});
    }
    else
    {
      this.bookingwzrd.timeSlotData.emit({"selectedDate":selectedDate,"selectedTime":this.selectedTimeslot}) ;
    }
    
  }

  getTimeByTherapistService(selectedService,selectedTherapist)
  {
    if(selectedService !=undefined)
    {
      console.log("Obj === "+selectedService);
      this.serviceId = selectedService.id ;
    }

    if(selectedTherapist !=undefined)
    {
      
      this.therapistId = selectedTherapist.id ;
    }
    this.config = this.commonprovider.getConfig();
    
    //get current month and year to insitalie first time 
    let currentDate = new Date();
    let selectedMonth = ("0"+(currentDate.getMonth()+1)).slice(-2) ;
    
    this.getCalendarMeta(selectedMonth,currentDate.getFullYear());
    
  }

  getCalendarMeta(selectedMonth,selectedYear) 
  {
    this.commonprovider.showLoader(this.label.getTimemsg);
    this.commonprovider
      .get(
        this.appsettings.appointmentApi
        + '?uid=' + this.config.therapist
        + '&sid=' + this.serviceId
        + '&year=' + selectedYear
        + '&month=' + selectedMonth
        + '&eid=' + this.therapistId
      )
      .subscribe(
        (resp: any) => {
          if (!resp.err_code && resp.data) {
            let employee = resp.data.employees[0]; // Hard code
            if(employee)
            {
              let disableWeeks = employee.weekDays ;
              //this.calOptions.disableWeeks = disableWeeks ;
              //disableWeeks = [0,6] ;  

              
              let disableRest = employee.rest_dates ;

              //let disableRest = [5,20,24] ;
              
              let _daysConfig: DayConfig[] = [];
              
              if(disableRest.length != 0)
              {
                disableRest.forEach(day => {
                  _daysConfig.push({
                    date: new Date(selectedYear, (selectedMonth-1), day),
                    disable:true
                  })
                });
  
                this.calOptions.daysConfig = _daysConfig;
                

                //reinitalized options 
                this.calOptions = {
                  /*autoDone: true,
                  pickMode: this.config.pickMode,
                  title: this.config.title,
                  closeLabel: this.config.closeLabel,
                  doneLabel: this.config.doneLabel,
                  from: startDate,
                  to: endDate,
                  defaultScrollTo: new Date(),
                  //defaultDateRange: this.dateRange,
                  //disableWeeks: this.config.disableWeeks
                  //     daysConfig: _daysConfig
                */
                disableWeeks:disableWeeks ,
                daysConfig:_daysConfig,
              
                };
                
              }
              else
              {
                this.calOptions.daysConfig = _daysConfig;
                

                //reinitalized options 
                this.calOptions = {
                  /*autoDone: true,
                  pickMode: this.config.pickMode,
                  title: this.config.title,
                  closeLabel: this.config.closeLabel,
                  doneLabel: this.config.doneLabel,
                  from: startDate,
                  to: endDate,
                  defaultScrollTo: new Date(),
                  //defaultDateRange: this.dateRange,
                  //disableWeeks: this.config.disableWeeks
                  //     daysConfig: _daysConfig
                */
                disableWeeks:disableWeeks ,
                daysConfig:_daysConfig,
              
                };

                
              }
              
            }else{
              this.timeNotFound = true;
            }
            this.commonprovider.hideLoader();
          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(resp.err_msg);
          }
        },
        error => {
          this.commonprovider.showToast(error.message);
          this.commonprovider.hideLoader();
        }
      );
  }

  //https://stackoverflow.com/questions/56269503/how-to-make-scroll-buttons-visible-automatically-when-number-of-records-increase
  slideNext()
  {

  }


  slidePrev()
  {

  }
  
}
