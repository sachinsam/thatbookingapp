import { Component, OnInit, Input } from '@angular/core';
import { AppSettings } from 'src/app/services/app-settings';
import { CommonProvider } from 'src/app/services/common/common';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-appinfo',
  templateUrl: './appinfo.component.html',
  styleUrls: ['./appinfo.component.scss'],
})
export class AppinfoComponent implements OnInit {

  userData: any;
  label: any;
  keyName: any;

  @Input() type: string;

  public passedObj:any;


  constructor(public appsettings: AppSettings,
    public commonprovider: CommonProvider,public modal:ModalController , public route:ActivatedRoute,
    private router: Router,public location:Location) 
    { 

      /**
       * if we would have taken navparam then we get the value her 
       *
      this.keyName = this.type;
      this.label = this.commonprovider.getLabels().info[this.keyName];
      */

     this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation() != null && this.router.getCurrentNavigation().extras.state) {
        this.passedObj = this.router.getCurrentNavigation().extras.state ; 
        this.type = this.router.getCurrentNavigation().extras.state.userdata.type;
      }
    });
  }

  ngOnInit() 
  {
    /**
     * sine input is taken we catch type input in init 
     */
    this.keyName = this.type;
      this.label = this.commonprovider.getLabels().info[this.keyName];
    this.initData();

  }

  initData() 
  {
    this.userData = this.commonprovider.getConfig()[this.label.keyName];
  }


  dismiss(){
    this.modal.dismiss();
  }

  goToBackUrl()
  {
    if(this.passedObj != undefined)
      this.location.back();
     else
      this.dismiss(); 
  }
}
