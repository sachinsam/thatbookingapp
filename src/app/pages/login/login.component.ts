import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { AppinfoComponent } from './appinfo/appinfo.component';
import { SignupComponent } from './signup/signup.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  //loginForm : FormGroup;
  isSubmitted : boolean=false;

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  // usrRole: any;
  loginData: any;
  label: any;
  config: any = {
    small_logo: 'assets/imgs/yoga.jpg'
  };

  options: InAppBrowserOptions = {
    location: 'no',//Or 'no'
    clearcache: 'yes',
    zoom: 'no',//Android only ,shows browser zoom controls
    hardwareback: 'yes',
    closebuttoncaption: 'Close', //iOS only
    // disallowoverscroll: 'no', //iOS only
    // toolbar: 'yes', //iOS only
    // enableViewportScale: 'no', //iOS only
    // allowInlineMediaPlayback: 'no',//iOS only
    // presentationstyle: 'pagesheet',//iOS only
  };

  fcmToken: any;
  devicePlatform: any;
  packgName: any;

  lgnFlow="login";

  constructor(private fb: FormBuilder,
    public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public storage: Storage,
    public iab: InAppBrowser,
    public router:Router,
    public modalController: ModalController
    ) 
    { 
      this.label = this.commonprovider.getLabels().login;

    }


  gotoDashboard()
  {
    alert("Testing button click") ;
    this.router.navigate(['./pages/dashboard']);
  }

  ngOnInit() 
  {
    console.log('FCM token ', this.commonprovider.getFCMToken());
    console.log('device platform ', this.commonprovider.getdevicePlatform());
    console.log('package name ', this.commonprovider.getPackageName());
    this.config = this.commonprovider.getConfig();
    this.fcmToken = this.commonprovider.getFCMToken();
    this.devicePlatform = this.commonprovider.getdevicePlatform();
    this.packgName = this.commonprovider.getPackageName();


    //by veramatic for simulation
    //this.devicePlatform = "android";
    //this.packgName = "com.limedrive.tp467";

    //const tmp = this.commonprovider.global.getuserDate();
    //this.commonprovider.setUserData(tmp);
  }

  onSubmit(lgnFrm) 
  {

    let dataObj = {
      'username': this.loginForm.value.username,
      'password': this.loginForm.value.password,
      'fcmToken': this.fcmToken,
      'platform': this.devicePlatform,
      'packageName': this.packgName
    }
    console.log("login form value ", dataObj);
    this.commonprovider.showLoader(this.label.dashboard);

    this.commonprovider.post(this.appsettings.logIn, dataObj).subscribe(
      (response: any) => {
        if (!response.err_code) {
          this.commonprovider.setUserData(response.data);
          //this.navCtrl.setRoot(DashboardPage, { userdata: response.data }); //commented by prashant use routing
          localStorage.setItem("usrLoggedIn","true");
          this.commonprovider.hideLoader();
          let navigationExtras: NavigationExtras = {
            state: {
              userdata: response.data
            }
          };
 
          if(this.lgnFlow == "login")
          {
            this.router.navigate(["pages","dashboard"],navigationExtras);
          }
          else
          {
            //when from booking ,it shud jsut dissmiss the screend
            this.closeModal();
          }
          
        } else {
          this.commonprovider.showToast(response.err_msg, 5000);
          this.commonprovider.hideLoader();
        }
      },
      err => {
        this.commonprovider.hideLoader();
        this.commonprovider.showToast(err.message || this.label.errMsg);
      }
    );
  }
  
  async gotoSignup() 
  {
    //this.router.navigate(['signup/']);
    // this.navCtrl.push(SignupPage);

   const modal = await this.modalController.create({
      component:SignupComponent,
    });

    return await modal.present();

  }

  gotoSkipfornow() 
  {
    //this.router.navigate(['signup/']);
    // this.navCtrl.push(SignupPage);

    //create sample userData for guest login
    localStorage.setItem("usrLoggedIn","true");
          
    let usrData = JSON.parse('{"auth":"","code":"200","id":"","user_role":0,"fcm_success":"0","jwt":"","name":"Guest","username":"guest","email":"guest@mailinator.com","password":"","password_clear":"","block":"0","sendEmail":"0","registerDate":"2019-11-05 16:45:30","lastvisitDate":"0000-00-00 00:00:00","activation":"","guest":0,"lastResetTime":"0000-00-00 00:00:00","resetCount":"0","requireReset":"0","otpKey":"","otep":"","user_info":{"id":"86","user_id":"0","order_name":"Guest","order_email":"guest.mil.com","order_phone":"null","order_country":"null","order_city":"null","order_state":"null","order_zip":null,"order_address":"null"}}');
    let navigationExtras: NavigationExtras = {
      state: {
        userdata: usrData
      }
    };

    this.router.navigate(["pages","dashboard"],navigationExtras);
    
    //return await modal.present();

  }

  forgotPassword() {
    let theOtherUrl: any = 'http://thatbooking.limedrive.co.uk/thatbooking/';
    const browser =
      this.iab
        .create(this.config.forgot_pwd_url, "_blank", this.options); //no spaces allowed in Options!

    browser.on('loadstart').subscribe((e) => {
      if (e.url == theOtherUrl) {
        browser.close();
        this.commonprovider.showToast(this.label.passwordResetText);
      }
    });
  }

  async showAppInfo() 
  {
    let navigationExtras: NavigationExtras = {
      state: {
        'type': 'info'
      }
    };

    //this.router.navigate(['/login/info']);
    //this.navCtrl.push(InfoPage, { 'type': 'info' });
    const modal = await this.modalController.create({
          component:AppinfoComponent,
          componentProps:{'type':'info'}
        });

        return await modal.present();
   
  }

  
  closeModal() {
    this.modalController.dismiss("login successfull");
  }

  closeModalBook() {
    this.modalController.dismiss();
  }

}
