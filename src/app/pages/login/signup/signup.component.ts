import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { ModalController } from '@ionic/angular';
import { TermComponent } from '../term/term.component';
import { AppComponent } from 'src/app/app.component';
import { AppinfoComponent } from '../appinfo/appinfo.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  signupForm = this.fb.group({
    // email: ['', Validators.required],
    // username: ['', Validators.required],
    // mobile: ['', Validators.required],
    // gender: ['', Validators.required],
    email: ['', Validators.compose([
      Validators.required,
      Validators.pattern("[^ @]*@[^ @]*")
    ])],
    password: ['', Validators.required],
    name: ['', Validators.required],
    terms: [true, Validators.required],
    uid: ['', Validators.required]
  });

  userRegister: any;
  label: any;
  config: any;

  constructor(private fb: FormBuilder,
    public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public modalcntrll:ModalController) 
    { 

    }

  ngOnInit() 
  {
    this.label = this.commonprovider.getLabels().signup;
    this.config = this.commonprovider.getConfig();
    this.signupForm.get('uid').setValue(this.config.therapist);
  }

  onSubmit() {
    if (this.signupForm.value.terms) {
      this.commonprovider.showLoader(this.label.userRegistration);

      this.commonprovider
        .post(this.appsettings.singupService, this.signupForm.value)
        .subscribe((response: any) => {
          this.commonprovider.hideLoader();
          if (!response.err_code) {
            this.userRegister = response.data;
            this.commonprovider.showToast(this.userRegister.message, 5000);
            //this.navCtrl.pop(); //close here
            this.modalcntrll.dismiss();
          } else {
            this.commonprovider.showToast(response.err_msg);
          }
        }, (err) => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        });
    } else {
      this.commonprovider.showToast(this.label.msgTerms);
    }
  }

  

  async showTerms() 
  {
    
    // this.navCtrl.push(InfoPage, { 'type': 'terms' });

   const modal = await this.modalcntrll.create({
      component:AppinfoComponent,
      componentProps:{'type':'terms'}
    });

    return await modal.present();

  }


  dismiss(){
    this.modalcntrll.dismiss();
  }


}
