import { Component, OnInit } from '@angular/core';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss'],
})
export class SuccessComponent implements OnInit {
  label: any;
  profile: any = {};
  successIcon: any;
  errorIcon: any;
  errIcnSvg;
  public passedObj:any;

  constructor( public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public route:ActivatedRoute,
    private router: Router,public location:Location,private sanitizer: DomSanitizer) 
    { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation() !=null && this.router.getCurrentNavigation().extras.state) {
          this.passedObj = this.router.getCurrentNavigation().extras.state;
          this.profile = this.passedObj.paymentResponse;

          
        }
      });

          this.label = this.commonprovider.getLabels().success;
          this.successIcon = this.commonprovider.getSvgIcos().success;
          this.errorIcon = this.commonprovider.getSvgIcos().error;
          this.errIcnSvg = this.sanitizer.bypassSecurityTrustHtml(this.errorIcon.data);

          
    }

  ngOnInit() 
  {
    this.label = this.commonprovider.getLabels().success;

    this.successIcon = this.commonprovider.getSvgIcos().success;
    this.errorIcon = this.commonprovider.getSvgIcos().error;

    //this.profile = this.navParams.get('paymentResponse');

    if(this.passedObj != undefined)
    {
      this.profile = this.passedObj.paymentResponse;
    }
    
  }

  goToHome() {
    //this.navCtrl.remove(1, 2);
    this.router.navigate(["pages","dashboard"]);

  }

  retryBooking() {
    //this.navCtrl.pop();
    this.location.back();
  }
}
