export class AppSettings {

  public API_ENDPOINT = 'https://thatbooking.limedrive.co.uk/thatbooking/api'; // Base URL

  public logIn = '/users/login';
  public getAppConfig = '/appointment/app';
  public getServices = '/appointment/services';
  public getBookingList = '/appointment/booking';
  public getCountryList = '/appointment/bookingdata';
  public singupService = '/users/user';
  public getTherapistsList = '/appointment/employee';
  public userProfile = '/users/user';
  public postBooking = '/appointment/booking';
  public getAppEmployee = '/appointment/employee';
  public contactTherapist = '/appointment/contact';
  public profileUpdate = '/appointment/user';
  public aboutInfo = '/appointment/app';

  public getAppointment = '/therapist/booking';
  public getBlockout = '/therapist/booking?blockout=1';
  public appLogout = '/users/logout';

  //Words
  public bookLoader = 'Booking your appointment';
  public serviceLoader = 'Fetching Services';
  public bookingLoader = 'Fetching Booking History';
  public countryLoader = 'Fetching Countries';

  public appointmentLoader = 'Fetching Appointments';

  //calendar
  public appointmentApi = '/appointment/employee' ;


  set_API_ENDPOINT(url) {
    this.API_ENDPOINT = url;
  }
}
