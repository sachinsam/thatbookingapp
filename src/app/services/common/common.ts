import { Injectable, EventEmitter } from '@angular/core';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { AppSettings } from '../app-settings';
import { GlobalProvider } from '../global/global';
import { Observable } from 'rxjs';
import { Network } from '@ionic-native/network/ngx';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';  // RequestOptions
import { Toast } from '@ionic-native/toast/ngx';
import { Storage } from '@ionic/storage';



/*


import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
*/

@Injectable()
export class CommonProvider {
  loader: any;
  toaster:any;
  userData: any;
  loading: any = false;
  isOnline: boolean = true;
  headers: any;
  isiOSplt: any = false;
  public loggedInStatus = JSON.parse(localStorage.getItem("usrLoggedIn") || 'false');

  public guestToLoggedin =new EventEmitter<any>();
  
  constructor(
    public loadingCtrl: LoadingController,
    public toast: ToastController,
    public alertCtrl: AlertController,
    public network: Network,
    public http: HttpClient,
    public toasterpopup: Toast,
    public appsettings: AppSettings,
    public global: GlobalProvider,
    private storage: Storage
  ) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.network.onConnect().subscribe(
      data => {
        this.displayNetworkUpdate(data.type);
      },
      error => { }
    );
      
    this.network.onDisconnect().subscribe(
      data => {
        this.displayNetworkUpdate(data.type);
      },
      error => { }
    );
  }

  async showLoader(msg?) {
    if (!this.loading) 
    {
      this.loading = true;
      
      this.loader = await this.loadingCtrl.create({
                            message: msg || 'Please wait...'
                          })

      this.loader.present();
    } 
  }

  async hideLoader() 
  {
    if (this.loading) {
      this.loading = false;
      await this.loader.dismiss();
      
    } else {
    }
  }

  async showToast(msg, time?, position?) {
    this.toaster = await this.toast.create({
      message: msg,
      duration: time || 2000,
      position: position || 'bottom'
    });
    this.toaster.onDidDismiss(() => {
      console.log('Dismissed toast');
     });

    this.toaster.present();
  }

  async showAlert(title: string, msg: string) {
    let alert = await this.alertCtrl.create({
      header: title,
      message: msg,
      buttons: ['Close']
    });
    alert.present();
  }

  public Alert = {
    confirm: (msg?, title?) => {
      return new Promise(async (resolve, reject) => {
        let alert = await this.alertCtrl.create({
          header: title || 'Confirm',
          message: msg || '',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                reject(false);
              }
            },
            {
              text: 'Ok',
              handler: () => {
                resolve(true);
              }
            }
          ]
        });
        alert.present();
      });
    },
    alert: async (msg, title?) => {
      let alert = await this.alertCtrl.create({
        header: title || 'Alert',
        subHeader: msg,
        buttons: ['Dismiss']
      });
      alert.present();
    }
  };

  displayNetworkUpdate(connectionState: string) {
    if (connectionState == 'online') {
      this.isOnline = true;
    } else {
      this.isOnline = false;
    }
    this.showToast('You are now ' + connectionState, 2500);
  }

  get(urlParam: any, options: any = this.headers): Observable<any> {
    return this.http
      .get(this.appsettings.API_ENDPOINT + urlParam, options)
      //.map(response => response.json());
  }

  post(
    urlParam: any,
    dataOb?: any,
    options: any = this.headers
  ): Observable<any> {
    let formObj = new FormData();
    for (let key in dataOb) {
      formObj.append(key, dataOb[key]);
    }
    return this.http
      .post(this.appsettings.API_ENDPOINT + urlParam, formObj, options)
      //.map(response => response.json());
  }

  //common console function
  console(pmsg: string = '', obj: any = '') {
    //  console.log(pmsg, obj);
    //  return true;
  }

  showToaster(msg?: any, tm: any = '5000', position: any = 'center') {
    this.toasterpopup.show(msg, tm, position).subscribe(toast => { });
  }

  getColorCode(variableName: string) {
    return getComputedStyle(document.documentElement)
      .getPropertyValue(variableName);
  }

   setUserData(user: any) {
    this.userData = user;
    this.setUserLoginId(user.id);
    const result =  this.storage.set('userData', user);
    console.log('set string in storage: ' + result);
    localStorage.setItem('userData', JSON.stringify(user)); //veramatic
  }

  setUserLoginId(id: any) {
    this.global.userId = id;
  }

  getUserLoginId() {
    let usr = this.storage.get('userData');
    return this.global.userId;
  }

  setConfig(config: any) {
    this.global.config = config;
    this.storage.set("configData",config) ; //by veramatic for simulation purpose 
  }

  getConfig() {
    return this.global.config; //since this is returning null on login page .. will debug later
    //return this.storage.get("configData");
  }

  setLabels(label) {
    this.global.label = label;
  }

  getLabels() {
    return this.global.label;
  }

  setSvgIcos(svgIcons) {
    this.global.svgIcons = svgIcons;
  }

  getSvgIcos() {
    return this.global.svgIcons;
  }

  getUserData() 
  {
    let  result:any=[]  ;
    /*this.storage.get('userData').then((val) => {
      //console.log('Your age is', val);
      result = val ;

      
      if(result["user_role"] != undefined)
        return result ;
      else
        return localStorage.getItem('userData');  

    });
*/
    //const result =  this.storage.get('userData');

    if(localStorage.getItem('userData') && localStorage.getItem('userData') != undefined)
      return JSON.parse(localStorage.getItem('userData')); 
     else
      return null ;  

  }

  getUserDataGlobal() {
    return this.userData;
  }

  async  removeStorage() 
  {
    localStorage.removeItem('userData');
    //localStorage.removeItem('userData');
    localStorage.setItem('usrLoggedIn',"false") ;
    
    return await  this.storage.remove('userData');
  }

  setisiOSplt(flag) {
    this.isiOSplt = flag;
  }

  getisiOSplt() {
    return this.isiOSplt;
  }

  setFCMToken(token:any){
    this.global.fcmToken = token;
  }

  getFCMToken(){
    return this.global.fcmToken;
  }

  setPackageName(pckgName: any, plt: string){
    this.global.packageName = pckgName;
    this.global.devicePlatform = plt;
  }

  getPackageName(){
    return this.global.packageName;
  }

  getdevicePlatform(){
    return this.global.devicePlatform;
  }

}
